# Evictions
resource "aws_cloudwatch_metric_alarm" "evictions" {
  alarm_name          = "[${var.account_name}] [elc] [${var.env}] ${var.elc_name} - Evictions"
  comparison_operator = "${var.evictions_comparison_operator}"
  evaluation_periods  = "${var.evictions_evaluation_periods}"
  metric_name         = "Evictions"
  namespace           = "AWS/ElastiCache"
  period              = "${var.evictions_period}"
  statistic           = "${var.evictions_statistic}"
  threshold           = "${var.evictions_threshold}"
  unit                = "${var.evictions_unit}"
  alarm_actions       = ["${var.alarm_actions}"]
  dimensions {
    CacheClusterId = "${var.elc_name}"
  }
}

output "evictions" {
  value = {
    id = "${aws_cloudwatch_metric_alarm.evictions.id}"
  }
}
